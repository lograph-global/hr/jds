# Customer Reliability Engineer

CS/Salesと連携しサービス利用者様のお困り事を技術で解決するための支援をしていただきます。具体的には以下のような内容が多く見られます。

- OmniDataBank の導入支援
    - お客様のご要望にあわせた要件を元にユーザー・アカウント登録・支援を行う
- OmniDataBank 計測タグの動作確認
    - ブラウザの開発者ツールを用いてお客様が設置された計測タグがうまく動かない等といったことに対して調査を行う
- OmniDataBank に登録されている観測設定の確認
    - 導入されている既存のお客様の設定の確認及び修正等
- OmniDataBank に関するその他確認
    - 仕様確認等、多岐にわたってCS及びお客様からの質問に対する確認・回答を行う
- その他、お客様、ベンダー及び連携先等との調整
    - Zoom等でオンラインミーティングでの調整等を行う
- その他、改善等

## Reference

* [Skill and Mind](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/skill_mind.md)
* [Stack](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/stack.md)
* [Environment](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/env.md)
