# Stack

## Backend

- [BEAR.Sunday](https://bearsunday.github.io/manuals/1.0/ja/)
- php
- MongoDB
- Redis
- Docker, docker-compose

## Frontend

- React.js
- CoreUI for Bootstrap
- Docker, docker-compose (optional)
