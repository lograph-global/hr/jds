# Environment

- Elasticbeanstalk
- EC2
    - Amazon Linux
    - Debian/Ubuntu
- Elasticache(Redis)
- Apache
- S3
- Cloudfront
- API Gateway
- Route53
- Cloudflare
- GitLab
- Slack
- Google Workspace
