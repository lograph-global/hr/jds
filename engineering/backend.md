# Backend Developer

OmniDataBankのバックエンド、API開発をお任せします。

## Stack

- [BEAR.Sunday](https://bearsunday.github.io/manuals/1.0/ja/)
- php
- MongoDB
- Redis
- Docker, docker-compose

## Reference

* [Skill and Mind](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/skill_mind.md)
* [Stack](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/stack.md)
* [Environment](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/env.md)
