# Frontend Developer

OmniDataBank のコンソール（管理画面）や計測タグの開発をお任せします。

## Stack

- React.js
- CoreUI for Bootstrap
- Docker, docker-compose (optional)

## Reference

* [Skill and Mind](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/skill_mind.md)
* [Stack](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/stack.md)
* [Environment](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/env.md)
