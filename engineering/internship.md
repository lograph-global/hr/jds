# Internship

好奇心旺盛で技術や広告に関して学んでみたいというインターンを募集しております。

## Reference

* [Skill and Mind](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/skill_mind.md)
* [Stack](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/stack.md)
* [Environment](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/env.md)
