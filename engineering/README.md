# Engineering

## Mission

オムニチャネルトラッキングサービス OmniDataBank のシステム開発を行っていただきます。

> ログラフが提供する OmniDataBank は日本のコールトラッキングのトップランナー  

OmniDataBankが提供するコールトラッキング機能は数あるオムニチャネルトラッキングの中の1例に過ぎません。まだまだ開拓の余地のあるチャネルは存在しますし、積極的に他のチャネルとの連携・拡大・拡張を続けています。チャネル拡大とは事業拡大を意味しており、事業拡大に向けて一緒に働いてくださるWeb Developerを募集しております。  

ログラフでは柔軟かつフラットに社内を横断的に日々課題を解決して、お互いにカバーしあって仕事をしています。  
強いて定義するなら以下のようなものになります。[^1]

## Roles

1. [Customer Reliability Engineer](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/cre.md)
2. [Service Reliability Engineer](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/sre.md)
3. [Backend Developer](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/backend.md)
4. [Frontend Developer](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/frontend.md)
5. [Internship](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/internship.md)

## Reference

* [Skill and Mind](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/skill_mind.md)
* [Stack](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/stack.md)
* [Environment](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/env.md)

[^1]: 全てのタスクはGitLabのIssueベースで動いています。
