# Service Reliability Engineer

OmniDataBank の安定的な稼働を目的とした Infrastructure 等を管理するために課題の抽出から解決までをお任せします。

- OmniDataBank のサービス監視
- OmniDataBank の環境設定の保守
- OmniDataBank のBackendのDeploy
- OmniDataBank のFrontendのDeploy
- OmniDataBank のTracker（計測タグ）のDeploy
- その他、改善等

## Reference

* [Skill and Mind](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/skill_mind.md)
* [Stack](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/stack.md)
* [Environment](https://gitlab.com/lograph-global/hr/jds/-/blob/main/engineering/env.md)
