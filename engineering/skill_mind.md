# Skill and Mind

全てを満たしている必要はありません :bow:

| Skill/Mind                      | Role    | Must | Want |
|---------------------------------|:-------:|:----:|:----:|
| 仲間を尊敬し尊重する               | 共通     |  ○   |      |
| 仲間じゃない人も尊敬し尊重する       | 共通     |      |  ○   |
| 自身で事象を調査し、解決まで導く     | 共通     |  ○   |      |
| 前向きに学びチャレンジする姿勢       | 共通     |  ○   |      |
| Web広告やWebトラッキングに関する知識 | 共通     |      |  ○   |
| Markdown等によるドキュメンテーション | 共通     |  ○   |      |
| Docker/docker-compose           | 共通     |  ○   |      |
| Linuxコマンドでの作業              | 共通     |  ○   |      |
| php                             | Backend  |  ○   |      |
| BEAR.Sunday                     | Backend  |      |  ○   |
| MongoDB                         | Backend  |      |  ○   |
| Redis                           | Backend  |      |  ○   |
| Node.js                         | Frontend |  ○   |      |
| JavaScript                      | Frontend |      |  ○   |
| React.js                        | Frontend |  ○   |      |
| CoreUI                          | Frontend |      |  ○   |
